class Weather {
  final String description;
  final int temp;
  final int feelsLikeTemp;
  final String iconImageUrl;

  Weather({
    required this.description,
    required this.temp,
    required this.feelsLikeTemp,
    required this.iconImageUrl,
  });
}
