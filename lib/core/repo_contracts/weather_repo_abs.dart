import 'package:weather_man/core/entities/weather.dart';

abstract class WeatherRepoAbs {
  Future<Weather> getWeather({required String inputText});
}