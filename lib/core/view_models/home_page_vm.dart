import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_man/common.dart';
import 'package:weather_man/core/entities/weather.dart';
import 'package:weather_man/core/repo_contracts/weather_repo_abs.dart';
import 'package:weather_man/core/failure.dart';

class HomePageVM extends ChangeNotifier {
  HomePageVM({required WeatherRepoAbs weatherRepo})
      : _weatherRepo = weatherRepo;

  final WeatherRepoAbs _weatherRepo;
  String _currentInputText = '';
  PageState _pageState = PageState.initial;
  Weather? _weather;
  String? _errorMessage;

  PageState get pageState => _pageState;

  Weather? get weather => _weather;

  String? get errorMessage => _errorMessage;

  set currentInputText(String text) => _currentInputText = text;

  Future<void> getWeather() async {
    _setState(PageState.loading);

    try {
      _weather = await _weatherRepo.getWeather(inputText: _currentInputText);
      _saveInputTextToSharedPrefs();
    } on Failure catch (f) {
      _errorMessage = f.errorMessage;
    } catch (_) {
      _errorMessage = 'Something went wrong!';
    }

    _setState(PageState.loaded);
  }

  void _setState(PageState state) {
    _pageState = state;

    if (state == PageState.loading) {
      _weather = null;
      _errorMessage = null;
    }

    notifyListeners();
  }

  void _saveInputTextToSharedPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(PrefKeys.lastInputText, _currentInputText);
  }
}

enum PageState { initial, loading, loaded }
