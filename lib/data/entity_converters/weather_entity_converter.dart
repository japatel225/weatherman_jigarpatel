import 'package:weather_man/core/entities/weather.dart';

class WeatherEntityConverter {
  static Weather fromJson(Map map) {
    final iconCode = map['weather'][0]['icon'];

    return Weather(
      description: map['weather'][0]['description'],
      temp: map['main']['temp'].round(),
      feelsLikeTemp: map['main']['feels_like'].round(),
      iconImageUrl: 'http://openweathermap.org/img/wn/$iconCode@4x.png',
    );
  }
}
