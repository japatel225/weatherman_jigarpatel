import 'dart:convert';
import 'dart:io';
import 'package:weather_man/core/entities/weather.dart';
import 'package:weather_man/core/failure.dart';
import 'package:weather_man/core/repo_contracts/weather_repo_abs.dart';
import 'package:http/http.dart' as http;
import 'package:weather_man/data/entity_converters/weather_entity_converter.dart';

class WeatherRepoImpl implements WeatherRepoAbs {
  static const _apiKey = '51530a0cdc2801a6a0de0ae07c5aca1f';

  @override
  Future<Weather> getWeather({required String inputText}) async {
    final uri = Uri.parse(
      'https://api.openweathermap.org/data/2.5/weather',
    ).replace(
      queryParameters: {
        'q': inputText,
        'appid': _apiKey,
        'units': 'metric',
      },
    );

    late final Map json;
    try {
      final response = await http.get(uri);
      json = _getDataJson(response);
    } on SocketException {
      throw Failure('No internet connection!');
    }

    return WeatherEntityConverter.fromJson(json);
  }

  Map _getDataJson(http.Response response) {
    switch (response.statusCode) {
      case 200:
        return jsonDecode(response.body);
      case 404:
        throw Failure('City not found!');
      default:
        throw Exception();
    }
  }
}
