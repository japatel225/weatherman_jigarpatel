import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_man/core/view_models/home_page_vm.dart';
import 'package:weather_man/data/repos/weather_repo_impl.dart';
import 'package:weather_man/ui/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather Man',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: ChangeNotifierProvider(
        create: (_) => HomePageVM(weatherRepo: WeatherRepoImpl()),
        child: const HomePage(),
      ),
    );
  }
}
