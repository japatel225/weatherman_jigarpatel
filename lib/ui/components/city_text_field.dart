import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_man/common.dart';
import 'package:weather_man/core/view_models/home_page_vm.dart';

class CityTextField extends StatefulWidget {
  const CityTextField({Key? key}) : super(key: key);

  @override
  _CityTextFieldState createState() => _CityTextFieldState();
}

class _CityTextFieldState extends State<CityTextField> {
  final tc = TextEditingController();

  @override
  void initState() {
    tc.addListener(() => context.read<HomePageVM>().currentInputText = tc.text);
    getLastInputText(context);
    super.initState();
  }

  void getLastInputText(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final lastInputText = prefs.getString(PrefKeys.lastInputText) ?? '';
    tc.text = lastInputText;
    tc.selection = TextSelection.collapsed(offset: tc.text.length);

    if (lastInputText != '') context.read<HomePageVM>().getWeather();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: tc,
      decoration: const InputDecoration(
        hintText: 'Enter city name',
        label: Text('City Name'),
        floatingLabelBehavior: FloatingLabelBehavior.auto,
      ),
    );
  }
}
