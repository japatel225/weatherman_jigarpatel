import 'package:flutter/material.dart';
import 'package:weather_man/core/entities/weather.dart';
import 'package:weather_man/common.dart';

class WeatherWidget extends StatelessWidget {
  final Weather weather;

  const WeatherWidget({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          width: 130,
          height: 130,
          child: Image.network(weather.iconImageUrl),
        ),
        Text(
          weather.description.capitalize(),
          style: const TextStyle(fontSize: 25),
        ),
        const SizedBox(height: 10),
        Text(
          '${weather.temp}°C',
          style: const TextStyle(fontSize: 35),
        ),
        const SizedBox(height: 5),
        Text(
          'Feels like ${weather.feelsLikeTemp}°C',
          style: const TextStyle(fontSize: 20),
        ),
        const SizedBox(height: 20),
      ],
    );
  }
}
