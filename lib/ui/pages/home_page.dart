import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_man/core/view_models/home_page_vm.dart';
import 'package:weather_man/ui/components/city_text_field.dart';
import 'package:weather_man/ui/components/weather_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(title: const Text('Weather Man')),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraint) => SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: ConstrainedBox(
                constraints: BoxConstraints(minHeight: constraint.maxHeight),
                child: const IntrinsicHeight(child: PageBody()),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class PageBody extends StatelessWidget {
  const PageBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<HomePageVM>(
      builder: (_, vm, __) {
        return Column(
          children: [
            const CityTextField(),
            const SizedBox(height: 15),
            ElevatedButton(
              child: const Text('Go'),
              onPressed: vm.pageState == PageState.loading
                  ? null
                  : () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      vm.getWeather();
                    },
            ),
            Expanded(child: Center(child: _buildContentWidget(vm))),
          ],
        );
      },
    );
  }

  Widget _buildContentWidget(HomePageVM vm) {
    if (vm.pageState == PageState.initial) {
      return const SizedBox();
    } else if (vm.pageState == PageState.loading) {
      return const CircularProgressIndicator();
    } else {
      if (vm.errorMessage != null) {
        return Text(vm.errorMessage!, style: const TextStyle(fontSize: 20));
      } else {
        return WeatherWidget(weather: vm.weather!);
      }
    }
  }
}
